/*
Variables y funciones que verifican si estamos desde celular o pc
*/

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};


if( isMobile.any() ){
    console.log('Is mobile');
    function isOnlineMobile(){

            if(navigator.onLine){
                //Estamos en linea
                console.log('Tenemos conexion a internet');
                  
                
                
            }else{
                //No estamos en linea
                console.log('No tenemos conexion a internet');

              
         
            }
    }
    //  window.addEventListener('online', isOnlineMobile );
      //window.addEventListener('offline', isOnlineMobile );

     isOnlineMobile();
}else{
    //Corriendo en web (Desktop)
}